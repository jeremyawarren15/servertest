﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                IPAddress ip = IPAddress.Parse("192.168.1.149");
                int port = 8000;

                TcpListener myList = new TcpListener(ip, port);

                myList.Start();

                Console.WriteLine($"Server running - Port: {port}");
                Console.WriteLine($"Local end point: {myList.LocalEndpoint}");
                Console.WriteLine("Waiting for connections...");

                Socket s = myList.AcceptSocket();
                Console.WriteLine($"Connection obtained from: {s.RemoteEndPoint}");

                byte[] b = new byte[100];
                int k = s.Receive(b);
                Console.WriteLine("Recieved...");

                for (int i = 0; i < k; i++)
                {
                    Console.Write(Convert.ToChar(b[i]));
                }

                // Send a message back to the client
                ASCIIEncoding asen = new ASCIIEncoding();
                s.Send(asen.GetBytes("Automatic Message: String recieved by server!"));
                Console.WriteLine("\nAutomatic message sent");

                s.Close();
                myList.Stop();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error..... " + ex.StackTrace);
            }
        }
    }
}
