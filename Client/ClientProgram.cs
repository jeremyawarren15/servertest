﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpClient tcpclnt = new TcpClient();
                Console.WriteLine("Connecting...");

                tcpclnt.Connect("192.168.1.149", 8000);

                Console.WriteLine("Connected...");
                Console.Write("Enter the string to be sent: ");

                var str = Console.ReadLine();
                Stream stm = tcpclnt.GetStream();

                // Conveting message to ascii
                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(str);
                Console.WriteLine("Sending...");

                stm.Write(ba, 0,ba.Length);

                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);

                for (int i = 0; i < k; i++)
                {
                    Console.Write(Convert.ToChar(bb[i]));
                }

                tcpclnt.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ops! Error! " + ex.StackTrace);
            }
        }
    }
}
